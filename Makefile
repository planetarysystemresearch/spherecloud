# Makefile for SIRIS4
#
# Antti Penttil�, 2017

COMP     ?= gcc
EXENAME  ?= SphereCloud
MACH     ?= LINUX
OPTI     ?= 1
PROC     ?= native

ifeq ($(COMP),gcc)
  FC := gfortran
	FCFLAGS := -ffree-form -std=f2008 -Wall -Wno-maybe-uninitialized
	ifeq ($(OPTI),0)
	  FCFLAGS += -O0 -g -fcheck=all -ffpe-trap=invalid,zero,overflow,underflow
	else ifeq ($(OPTI),1)
	  FCFLAGS += -O2 -fcheck=all -ffpe-trap=invalid,zero,overflow,underflow
	else ifeq ($(OPTI),2)
	  FCFLAGS += -O3 -march=$(PROC) -ffast-math -funroll-loops
	endif
endif


SRC = poisson SphereCloud SphereCloud-main

# RECIPIES

all : main

main : $(addsuffix .o,$(SRC))
	$(FC) -o $(EXENAME) $(addsuffix .o,$(SRC))

%.o : %.f
	$(FC) -c $(FCFLAGS) $*.f

clean : 
	rm -rf *.o *.mod $(EXENAME)
