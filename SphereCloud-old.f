! Packing spheres randomly and evenly into a cloud. Limiting volume
! is either sphere or box. Either way, packing is done in
! periodic box to avoid edge effects in packing. The goal is
! to achieve even packing density in the volume. Due to this goal,
! in the spherical volume packing the number of spheres (and packing density)
! will change randomly a bit from one realization to another.
!
! In spherical cloud, packed spheres are either completely inside the 
! volume (4th command argument 0), or only centers inside the volume
! (4th command argument 1). In periodic box, only the centers of the
! spheres are in the box (4th command argument 2).
!
! Antti Penttilä, Department of Physics, Univ. of Helsinki
! 2017
!
PROGRAM SphereCloud

  USE, INTRINSIC :: iso_fortran_env
  IMPLICIT NONE
  
  INTEGER, PARAMETER :: dp = REAL64
  REAL(KIND=dp), PARAMETER :: PI = 3.141592653589793_dp
  
  INTEGER :: cac, i, j, k, geochoice, n, och, ninS, ninB, bordn
  REAL(KIND=dp) :: x, y, z, R, rr, pD, Bvol, Svol, ssvol, pDinS, pDinB, &
    rr2, R2, rr2p2
  REAL(KIND=dp), DIMENSION(:,:), ALLOCATABLE :: geom
  REAL(KIND=dp), DIMENSION(:,:,:), ALLOCATABLE :: borders
  CHARACTER(LEN=256) :: carg, ofn
  LOGICAL :: sphgeo = .TRUE., isok
  
  WRITE(*,*) ""
  WRITE(*,*) "Starting SphereCloud"
  WRITE(*,*) ""

 cac = COMMAND_ARGUMENT_COUNT()
  IF(cac < 4 .OR. cac > 5) THEN
    WRITE(*,*) "Error, needs four or five command arguments (R, r, pD, sphere(0/1)/box(2), [filename])"
    STOP
  END IF
  
  CALL init_rng()
  
  CALL GET_COMMAND_ARGUMENT(1, carg)
  READ(carg, *) R
  CALL GET_COMMAND_ARGUMENT(2, carg)
  READ(carg, *) rr
  CALL GET_COMMAND_ARGUMENT(3, carg)
  READ(carg, *) pD
  CALL GET_COMMAND_ARGUMENT(4, carg)
  READ(carg, *) geochoice
  IF(cac == 5) THEN
    CALL GET_COMMAND_ARGUMENT(5, ofn)
    och = 8
    OPEN(och, FILE=ofn, ACTION='write', STATUS='replace')
  ELSE
    WRITE(ofn, '(A)') "standard_output"
    och = OUTPUT_UNIT
  END IF
  
  rr2 = 2*rr
  R2 = 2*R
  rr2p2 = rr2**2

  Bvol = R2**3
  ssvol = 4.0_dp * PI * rr**3 / 3.0_dp
  pDinB = pD
  ninB = NINT(pDinB * Bvol / ssvol)
  IF(geochoice < 2) THEN
    sphgeo = .TRUE.
    Svol = 4.0_dp * PI * R**3 / 3.0_dp
  ELSE
    sphgeo = .FALSE.
  END IF

  WRITE(*,'(A,I0,A,F6.3,A,F9.3,A)') " Packing ", ninB, " spheres with r=", &
    rr, " to R=", R, " periodic box"
  WRITE(*,*) ""
  
  ALLOCATE(geom(ninB,3),borders(ninB,26,3))
  
  i = 1
  CALL randomPlace(x,y,z,R2)
  geom(i,:) = (/ x, y, z /)
  IF(isBorder(x,y,z,R2,rr)) THEN
    bordn = 1
    CALL addToBorder(borders(bordn,:,:),R2,x,y,z)
  ELSE
    bordn = 0
  END IF
  DO WHILE (i < ninB)
    IF(MOD(i,100) == 0) THEN
      WRITE(*,*) "Now at ", i
    END IF

    DO
      CALL randomPlace(x,y,z,R2)
      isok = checkPlace(x,y,z,R2,rr2,rr2p2,geom,i,borders,bordn)
      IF(isok) EXIT
    END DO
    
    i = i+1
    geom(i,:) = (/ x, y, z /)
    IF(isBorder(x,y,z,R2,rr)) THEN
      bordn = bordn+1
      CALL addToBorder(borders(bordn,:,:),R2,x,y,z)
    END IF

  END DO
  
  ! Re-use borders-list
  IF(sphgeo) THEN
    j = 0
    ! Spheres completely in or only centers?
    IF (geochoice == 0) THEN
      x = (R-rr)**2
    ELSE
      x = R**2
    END IF
    DO i=1,ninB
      IF((geom(i,1)-R)**2 + (geom(i,2)-R)**2 + (geom(i,3)-R)**2 .LE. x) THEN
        j = j+1
        borders(j,1,:) = geom(i,:)
      END IF
    END DO
    ninS = j
    pDinS = (ninS * ssvol) / Svol
  END IF
  
  IF(sphgeo) THEN
    WRITE(*,*) "Packing to spherical volume ready, writing result to '", TRIM(ofn), "'"
    WRITE(*,'(1X,I0,A,F5.3)') ninS, " spheres in volume, packing density ", pDinS
  ELSE
    WRITE(*,*) "Packing to periodic box ready, writing result to '", TRIM(ofn), "'"
    WRITE(*,'(1X,I0,A,F5.3)') ninB, " spheres in volume, packing density ", pDinB
  END IF
  
  IF(sphgeo) THEN
    DO i=1,ninS
      WRITE(och,*) rr, borders(i,1,:)
    END DO
  ELSE
    DO i=1,ninB
      WRITE(och,*) rr, geom(i,:)
    END DO
  END IF
  IF(och /= OUTPUT_UNIT) THEN
    CLOSE(och)
  END IF
  
  WRITE(*,*) ""
  WRITE(*,*) "Ready"
  WRITE(*,*) ""

CONTAINS


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION checkPlace(x,y,z,R2,rr2,rr2p2,geom,gi,borders,bordn) RESULT(ok)
  LOGICAL :: ok
  REAL(KIND=dp), INTENT(IN) :: x,y,z,R2,rr2,rr2p2
  REAL(KIND=dp), DIMENSION(:,:), INTENT(IN) :: geom
  REAL(KIND=dp), DIMENSION(:,:,:), INTENT(IN) :: borders
  INTEGER, INTENT(IN) :: gi,bordn
  INTEGER :: ii,jj
  
  ! Check non-periodic volume
  DO ii=1,gi
    IF((x-geom(ii,1))**2 + (y-geom(ii,2))**2 + (z-geom(ii,3))**2 < rr2p2) THEN
      ok = .FALSE.
      RETURN
    END IF
  END DO
  
  ! Need to check borders?
  IF(isBorder(x,y,z,R2,rr2)) THEN
    ! Yes
    DO ii=1,bordn
      DO jj=1,26
        IF((x-borders(ii,jj,1))**2 + (y-borders(ii,jj,2))**2 + &
          (z-borders(ii,jj,3))**2 < rr2p2) THEN
          ok = .FALSE.
          RETURN
        END IF
      END DO
    END DO
  END IF

  ok = .TRUE.

END FUNCTION checkPlace


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE addToBorder(bordUnit,R2,x,y,z)
  REAL(KIND=dp), DIMENSION(:,:), INTENT(OUT) :: bordUnit
  REAL(KIND=dp), INTENT(IN) :: R2,x,y,z

  bordunit(1,:) = (/ x-R2, y, z-R2 /)
  bordunit(2,:) = (/ x-R2, y-R2, z-R2 /)
  bordunit(3,:) = (/ x, y-R2, z-R2 /)
  bordunit(4,:) = (/ x+R2, y-R2, z-R2 /)
  bordunit(5,:) = (/ x+R2, y, z-R2 /)
  bordunit(6,:) = (/ x+R2, y+R2, z-R2 /)
  bordunit(7,:) = (/ x, y+R2, z-R2 /)
  bordunit(8,:) = (/ x-R2, y+R2, z-R2 /)
  bordunit(9,:) = (/ x, y, z-R2 /)
  bordunit(10,:) = (/ x-R2, y, z /)
  bordunit(11,:) = (/ x-R2, y-R2, z /)
  bordunit(12,:) = (/ x, y-R2, z /)
  bordunit(13,:) = (/ x+R2, y-R2, z /)
  bordunit(14,:) = (/ x+R2, y, z /)
  bordunit(15,:) = (/ x+R2, y+R2, z /)
  bordunit(16,:) = (/ x, y+R2, z /)
  bordunit(17,:) = (/ x-R2, y+R2, z /)
  bordunit(18,:) = (/ x-R2, y, z+R2 /)
  bordunit(19,:) = (/ x-R2, y-R2, z+R2 /)
  bordunit(20,:) = (/ x, y-R2, z+R2 /)
  bordunit(21,:) = (/ x+R2, y-R2, z+R2 /)
  bordunit(22,:) = (/ x+R2, y, z+R2 /)
  bordunit(23,:) = (/ x+R2, y+R2, z+R2 /)
  bordunit(24,:) = (/ x, y+R2, z+R2 /)
  bordunit(25,:) = (/ x-R2, y+R2, z+R2 /)
  bordunit(26,:) = (/ x, y, z+R2 /)

END SUBROUTINE addToBorder


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION isBorder(x,y,z,R2,rr) RESULT(isbord)
  LOGICAL :: isbord
  REAL(KIND=dp), INTENT(IN) :: x,y,z,R2,rr
  REAL(KIND=dp) :: sd
  
  sd = R2-rr
  ! Check if sphere is in the border zone
  isbord = .FALSE.
  IF(x < rr .OR. x > sd) THEN
    isbord = .TRUE.
    RETURN
  ELSE IF(y < rr .OR. y > sd) THEN
    isbord = .TRUE.
    RETURN
  ELSE IF(z < rr .OR. z > sd) THEN
    isbord = .TRUE.
    RETURN
  END IF

END FUNCTION isBorder


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE randomPlace(x,y,z, box)
  REAL(KIND=dp), INTENT(OUT) :: x, y, z
  REAL(KIND=dp), INTENT(IN) :: box
  
  CALL RANDOM_NUMBER(x)
  x = x*box
  CALL RANDOM_NUMBER(y)
  y = y*box
  CALL RANDOM_NUMBER(z)
  z = z*box

END SUBROUTINE randomPlace


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE init_rng()
  INTEGER :: ss
  INTEGER, DIMENSION(8) :: t
  INTEGER, DIMENSION(:), ALLOCATABLE :: seed
  REAL(KIND=dp) :: x

  CALL RANDOM_SEED(SIZE=ss)
  ALLOCATE(seed(ss))
  
  CALL DATE_AND_TIME(VALUES=t)
  seed(:) = 100*t(7) + t(8)/10

  CALL RANDOM_SEED(PUT=seed)
  CALL RANDOM_NUMBER(x)

END SUBROUTINE init_rng

  
END PROGRAM SphereCloud
