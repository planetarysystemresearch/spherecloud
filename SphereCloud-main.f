! Creating a uniform cloud of spheres with given [expected] packing density.
!
! Limiting volume can be either sphere or a box. In any case, the cloud
! is first created inside periodic box.
!
! One can pack with fixed number of particles in the initial box, or with
! Poisson-distributed number of particles. If spherical volume is extracted,
! the number of particles will be random in any case. With Poisson distribution,
! however, the variance of the particle number is correct inside the spherical volume.
! With fixed number of particles in the initial box, the variance of the particle number
! in the spherical volume is smaller.
!
! Antti Penttil�, Department of Physics, Univ. of Helsinki
! 2017
!
PROGRAM SphereCloudMain

  USE, INTRINSIC :: iso_fortran_env
 USE SphereCloud
  IMPLICIT NONE
  
  INTEGER, PARAMETER :: dp = REAL64
  REAL(KIND=dp), PARAMETER :: PI = 3.141592653589793_dp
  
  INTEGER :: cac, och, fixOrPois, geochoice, n, i
  REAL(KIND=dp) :: R, rr, nOrPD, pD
  REAL(KIND=dp), DIMENSION(:,:), POINTER :: sphlist => NULL()
  CHARACTER(LEN=256) :: carg, ofn
  
  WRITE(*,*) ""
  WRITE(*,*) "Starting SphereCloud"
  WRITE(*,*) ""

  cac = COMMAND_ARGUMENT_COUNT()
  IF(cac < 5 .OR. cac > 6) THEN
    WRITE(*,*) "Error, needs five or six command arguments (R, r, n/pD, fixed(0)/poisson(1), sphere(0,1)/box(2), [filename])"
    STOP
  END IF
  
  CALL init_rng()
  
  CALL GET_COMMAND_ARGUMENT(1, carg)
  READ(carg, *) R
  CALL GET_COMMAND_ARGUMENT(2, carg)
  READ(carg, *) rr
  CALL GET_COMMAND_ARGUMENT(3, carg)
  READ(carg, *) nOrPD
  CALL GET_COMMAND_ARGUMENT(4, carg)
  READ(carg, *) fixOrPois
  CALL GET_COMMAND_ARGUMENT(5, carg)
  READ(carg, *) geochoice
  IF(cac == 6) THEN
    CALL GET_COMMAND_ARGUMENT(6, ofn)
    och = 8
    OPEN(och, FILE=ofn, ACTION='write', STATUS='replace')
  ELSE
    WRITE(ofn, '(A)') "standard_output"
    och = OUTPUT_UNIT
  END IF

  CALL pack_spheres(R,rr,nOrPD,fixOrPois,geochoice,sphlist,n,pD)
  
  WRITE(*,*) "Packing to volume ready, writing result to '", TRIM(ofn), "'"
  WRITE(*,'(1X,I0,A,F5.3)') n, " spheres in volume, packing density ", pD
  WRITE(*,*) ""
  
  DO i=1,n
    WRITE(och,*) rr, sphlist(i,:)
  END DO
  IF(och /= OUTPUT_UNIT) THEN
    CLOSE(och)
  END IF
  
  WRITE(*,*) ""
  WRITE(*,*) "Ready"
  WRITE(*,*) ""

  
  
CONTAINS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE init_rng(ownseed)
  INTEGER, INTENT(IN), OPTIONAL :: ownseed
  INTEGER :: ss
  INTEGER, DIMENSION(8) :: t
  INTEGER, DIMENSION(:), ALLOCATABLE :: seed
  REAL(KIND=dp) :: x

  CALL RANDOM_SEED(SIZE=ss)
  ALLOCATE(seed(ss))
  
  IF(PRESENT(ownseed)) THEN
    seed(:) = ownseed
  ELSE
    CALL DATE_AND_TIME(VALUES=t)
    seed(:) = 100*t(7) + t(8)/10
  END IF

  CALL RANDOM_SEED(PUT=seed)
  CALL RANDOM_NUMBER(x)

END SUBROUTINE init_rng

END PROGRAM SphereCloudMain