# README #

### What is this repository for? ###

* Packing spheres randomly into a cloud with uniform distribution.
* Packing is done in periodic cubical volume.
* Spherical volume can be extracted
* Number of particles in the cubical volume can be fixed, or from Poisson distribution
* In spherical volume, one can select to either include complete spheres inside the volume, or the centers of the spheres. In the latter case, the number of the spheres in the volume is Poisson-distributed with expected value from the packing density and the volume given as input.
* 1.0

### How do I get set up? ###

* Fortran 2008-standard. Compile using GCC with options '-ffree-form -std=f2008'
* Makefile

### Who do I talk to? ###

* Repo owner Antti Penttil� (antti . i . penttila at helsinki . fi)