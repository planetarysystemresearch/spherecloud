! Creating a uniform cloud of spheres with given [expected] packing density.
!
! Limiting volume can be either sphere or a box. In any case, the cloud
! is first created inside periodic box.
!
! One can pack with fixed number of particles in the initial box, or with
! Poisson-distributed number of particles. If spherical volume is extracted,
! the number of particles will be random in any case. With Poisson distribution,
! however, the variance of the particle number is correct inside the spherical volume.
! With fixed number of particles in the initial box, the variance of the particle number
! in the spherical volume is smaller.
!
! Antti Penttilä, Department of Physics, Univ. of Helsinki
! 2017
!
MODULE SphereCloud

  USE, INTRINSIC :: iso_fortran_env
  USE poisson
  IMPLICIT NONE
  
  INTEGER, PARAMETER :: dp = REAL64
  REAL(KIND=dp), PARAMETER :: PI = 3.141592653589793_dp
  
  REAL(KIND=dp) :: R, rr, rr2, R2, rr2p2, Bvol, ssvol, pDinB, Svol
  INTEGER :: ninB, nPacked
  REAL(KIND=dp), DIMENSION(:,:), ALLOCATABLE :: geom
  REAL(KIND=dp), DIMENSION(:,:,:), ALLOCATABLE :: borders
  
  PRIVATE
  PUBLIC pack_spheres
  
CONTAINS

SUBROUTINE pack_spheres(pR, prr, nOrPD, fixOrPois, geochoice, sphlist, pn, pD)
  REAL(KIND=dp), INTENT(IN) :: pR, prr, nOrPD
  INTEGER, INTENT(IN) :: fixOrPois, geochoice
  REAL(KIND=dp), DIMENSION(:,:), POINTER, INTENT(OUT) :: sphlist
  INTEGER, INTENT(OUT) :: pn
  REAL(KIND=dp), INTENT(OUT) :: pD
  INTEGER :: expn
  REAL(KIND=dp) :: x, y
  
  R = pR
  rr = prr
  rr2 = 2*rr
  R2 = 2*R
  rr2p2 = rr2**2
  Bvol = R2**3
  ssvol = 4.0_dp * PI * rr**3 / 3.0_dp
  Svol = 4.0_dp * PI * R**3 / 3.0_dp
  
  ! Fixed number of spheres
  IF(fixOrPois == 0) THEN
    ! n is given
    IF(nOrPD > 0) THEN
      ninB = NINT(nOrPD)
      pDinB = ninB*ssvol/Bvol
    ! pD is given
    ELSE
      pDinB = -nOrPD
      ninB = NINT(pDinB * Bvol / ssvol)
    END IF
  ! Poisson-number of spheres
  ELSE
    ! n is given
    IF(nOrPD > 0) THEN
      expn = FLOOR(nOrPD)
      CALL RANDOM_NUMBER(x)
      IF(x < nOrPD-expn) THEN
        expn = expn+1
      END IF
      ninB = rnd_poisson(expn)
      pDinB = ninB*ssvol/Bvol
    ! pD is given
    ELSE
      pDinB = -nOrPD
      y = pDinB * Bvol / ssvol
      expn = FLOOR(y)
      CALL RANDOM_NUMBER(x)
      IF(x < y-expn) THEN
        expn = expn+1
      END IF
      ninB = rnd_poisson(expn)
      pDinB = ninB*ssvol/Bvol
    END IF 
  END IF
  
!  WRITE(*,'(A,I0,A,F9.3)') " Packing in box, ", ninB, " particles, pD in box ", pDinB
  CALL pack_in_box()
!  WRITE(*,'(A)') " Packing in box ready"
  
  IF(geochoice < 2) THEN
    CALL strip_spherical_volume(geochoice)
  END IF
  
  ALLOCATE(sphlist(nPacked,3))
  sphlist(:,:) = geom(1:nPacked,:)
  pn = nPacked
  IF(geochoice < 2) THEN
    pD = nPacked*ssvol/Svol
  ELSE
    pD = nPacked*ssvol/Bvol
  END IF
  
END SUBROUTINE pack_spheres


SUBROUTINE strip_spherical_volume(geochoice)
  INTEGER, INTENT(IN) :: geochoice
  INTEGER :: i, j
  REAL(KIND=dp) :: x
  
  ! Re-use borders-list
  j = 0
  ! Center in
  IF(geochoice == 0) THEN
    x = R**2
  ! Completely in
  ELSE
    x = (R-rr)**2
  END IF

  DO i=1,ninB
    IF((geom(i,1)-R)**2 + (geom(i,2)-R)**2 + (geom(i,3)-R)**2 .LE. x) THEN
      j = j+1
      borders(j,1,:) = geom(i,:)
    END IF
  END DO
  nPacked = j
  geom(1:nPacked,:) = borders(1:nPacked,1,:)

END SUBROUTINE strip_spherical_volume


SUBROUTINE pack_in_box()
  
  INTEGER :: i, bordn
  REAL(KIND=dp) :: x, y, z
  LOGICAL :: isok
  
  IF(ALLOCATED(geom)) DEALLOCATE(geom)
  IF(ALLOCATED(borders)) DEALLOCATE(borders)
  !WRITE(*,'(A,I0)') " ", ninB
  ALLOCATE(geom(ninB,3),borders(ninB,26,3))
  
  i = 1
  CALL randomPlace(x,y,z,R2)
  geom(i,:) = (/ x, y, z /)
  IF(isBorder(x,y,z,R2,rr)) THEN
    bordn = 1
    CALL addToBorder(borders(bordn,:,:),R2,x,y,z)
  ELSE
    bordn = 0
  END IF
  DO WHILE (i < ninB)

    DO
      CALL randomPlace(x,y,z,R2)
      isok = checkPlace(x,y,z,R2,rr2,rr2p2,geom,i,borders,bordn)
      IF(isok) EXIT
    END DO
    
    i = i+1
    geom(i,:) = (/ x, y, z /)
    IF(isBorder(x,y,z,R2,rr)) THEN
      bordn = bordn+1
      CALL addToBorder(borders(bordn,:,:),R2,x,y,z)
    END IF

  END DO
  
  nPacked = ninB

END SUBROUTINE pack_in_box


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION checkPlace(x,y,z,R2,rr2,rr2p2,geom,gi,borders,bordn) RESULT(ok)
  LOGICAL :: ok
  REAL(KIND=dp), INTENT(IN) :: x,y,z,R2,rr2,rr2p2
  REAL(KIND=dp), DIMENSION(:,:), INTENT(IN) :: geom
  REAL(KIND=dp), DIMENSION(:,:,:), INTENT(IN) :: borders
  INTEGER, INTENT(IN) :: gi,bordn
  INTEGER :: ii,jj
  
  ! Check non-periodic volume
  DO ii=1,gi
    IF((x-geom(ii,1))**2 + (y-geom(ii,2))**2 + (z-geom(ii,3))**2 < rr2p2) THEN
      ok = .FALSE.
      RETURN
    END IF
  END DO
  
  ! Need to check borders?
  IF(isBorder(x,y,z,R2,rr2)) THEN
    ! Yes
    DO ii=1,bordn
      DO jj=1,26
        IF((x-borders(ii,jj,1))**2 + (y-borders(ii,jj,2))**2 + &
          (z-borders(ii,jj,3))**2 < rr2p2) THEN
          ok = .FALSE.
          RETURN
        END IF
      END DO
    END DO
  END IF

  ok = .TRUE.

END FUNCTION checkPlace


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE addToBorder(bordUnit,R2,x,y,z)
  REAL(KIND=dp), DIMENSION(:,:), INTENT(OUT) :: bordUnit
  REAL(KIND=dp), INTENT(IN) :: R2,x,y,z

  bordunit(1,:) = (/ x-R2, y, z-R2 /)
  bordunit(2,:) = (/ x-R2, y-R2, z-R2 /)
  bordunit(3,:) = (/ x, y-R2, z-R2 /)
  bordunit(4,:) = (/ x+R2, y-R2, z-R2 /)
  bordunit(5,:) = (/ x+R2, y, z-R2 /)
  bordunit(6,:) = (/ x+R2, y+R2, z-R2 /)
  bordunit(7,:) = (/ x, y+R2, z-R2 /)
  bordunit(8,:) = (/ x-R2, y+R2, z-R2 /)
  bordunit(9,:) = (/ x, y, z-R2 /)
  bordunit(10,:) = (/ x-R2, y, z /)
  bordunit(11,:) = (/ x-R2, y-R2, z /)
  bordunit(12,:) = (/ x, y-R2, z /)
  bordunit(13,:) = (/ x+R2, y-R2, z /)
  bordunit(14,:) = (/ x+R2, y, z /)
  bordunit(15,:) = (/ x+R2, y+R2, z /)
  bordunit(16,:) = (/ x, y+R2, z /)
  bordunit(17,:) = (/ x-R2, y+R2, z /)
  bordunit(18,:) = (/ x-R2, y, z+R2 /)
  bordunit(19,:) = (/ x-R2, y-R2, z+R2 /)
  bordunit(20,:) = (/ x, y-R2, z+R2 /)
  bordunit(21,:) = (/ x+R2, y-R2, z+R2 /)
  bordunit(22,:) = (/ x+R2, y, z+R2 /)
  bordunit(23,:) = (/ x+R2, y+R2, z+R2 /)
  bordunit(24,:) = (/ x, y+R2, z+R2 /)
  bordunit(25,:) = (/ x-R2, y+R2, z+R2 /)
  bordunit(26,:) = (/ x, y, z+R2 /)

END SUBROUTINE addToBorder


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
FUNCTION isBorder(x,y,z,R2,rr) RESULT(isbord)
  LOGICAL :: isbord
  REAL(KIND=dp), INTENT(IN) :: x,y,z,R2,rr
  REAL(KIND=dp) :: sd
  
  sd = R2-rr
  ! Check if sphere is in the border zone
  isbord = .FALSE.
  IF(x < rr .OR. x > sd) THEN
    isbord = .TRUE.
    RETURN
  ELSE IF(y < rr .OR. y > sd) THEN
    isbord = .TRUE.
    RETURN
  ELSE IF(z < rr .OR. z > sd) THEN
    isbord = .TRUE.
    RETURN
  END IF

END FUNCTION isBorder


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
SUBROUTINE randomPlace(x,y,z, box)
  REAL(KIND=dp), INTENT(OUT) :: x, y, z
  REAL(KIND=dp), INTENT(IN) :: box
  
  CALL RANDOM_NUMBER(x)
  x = x*box
  CALL RANDOM_NUMBER(y)
  y = y*box
  CALL RANDOM_NUMBER(z)
  z = z*box

END SUBROUTINE randomPlace




  
END MODULE SphereCloud
