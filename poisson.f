MODULE poisson
!use common
  ! Poisson-distributed random number, algorithm for 'small' lambda (<~1000)
  ! From Wikipedia, Knuth algorithm
  
  IMPLICIT NONE
  INTEGER, PARAMETER :: dp = SELECTED_REAL_KIND(12)
  REAL(dp), PARAMETER :: PI=3.141592653589793238462
  
  PRIVATE
  PUBLIC rnd_poisson
  
contains


FUNCTION rnd_poisson(lambda) RESULT(k)
  INTEGER, INTENT(IN) :: lambda
  INTEGER :: k
  
  IF(lambda < 700) THEN
    k = poisson_small(lambda)
  ELSE
    k = poisson_large(lambda)
  END IF
  
END FUNCTION rnd_poisson

  
FUNCTION poisson_small(lambda) RESULT(k)
  INTEGER, INTENT(IN) :: lambda
  INTEGER :: k
  REAL(KIND=dp) :: u, p, L
  k = 0
  p = 1.0_dp
  L = EXP(-1.0_dp * lambda)

  DO WHILE(p > L)
    k = k+1
    CALL RANDOM_NUMBER(u)
    p = p*u
  END DO
  
  k = k-1
  
END FUNCTION poisson_small


FUNCTION poisson_large(lambda) RESULT(k)
  INTEGER, INTENT(IN) :: lambda
  INTEGER :: k
  REAL(KIND=dp) :: u, v, pi = 3.141592653589793_dp
  
  CALL RANDOM_NUMBER(u)
  CALL RANDOM_NUMBER(v)
  u = SQRT(-2 * LOG(u)) * COS(2*pi*v)
  v = lambda + u*SQRT(REAL(lambda,KIND=dp))
  k = NINT(v)

END FUNCTION poisson_large


end module
